import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  title = 'daytona';
  email = '';
  password = '';
  firstName = '';
  lastName = '';
  showLogin = true;

  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

}
