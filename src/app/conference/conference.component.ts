import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import Conference from '../model/Conference';
import {Observable} from 'rxjs';
import {ConferenceService} from '../services/conference.service';

@Component({
  selector: 'app-conference',
  templateUrl: './conference.component.html',
  styleUrls: ['./conference.component.scss']
})
export class ConferenceComponent implements OnInit {

  conferences$: Observable<Conference[]>;
  conferenceName: string;
  constructor(public auth: AuthService,
              public conferenceService: ConferenceService) { }

  ngOnInit(): void {
    /**
     * This is a very dense line of code given all that it's doing.  Every time a change is made to a conference, this observer exposes
     * it to the UI.
     */
    this.conferences$ = this.conferenceService.subscribeToConferences();
  }
}
