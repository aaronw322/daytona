import Mutation from '../Mutation';

/**
 * So I did make an extra effort to model some of the structure that I thought was important to demonstrate in no-sql land.  Notice that we
 * have a `version` field that every document inherits.  This is very important, b/c it allows us to keep track with the state of our
 * schema.  You can think of each `Entity` child class as the DDL for our database, where the `version` column acts like our "Liquibase",
 * giving us the infrastructure needed to migrate our schema as it evolves one document (row) at a time. The fact that you can migrate
 * the state of your schema one document at a time versus having to migrate an entire table at a time in relational land is a subtle, yet
 * very powerful feature of document-based data stores (if modelled right, I should say).
 */
export default abstract class Entity {
  id?: string;
  version?: string;
  created?: Mutation;
  changed?: Mutation;
  protected constructor(entity: Entity) {
    if (entity) {
      this.id = entity.id;
      this.created = entity.created;
      this.changed = entity.changed;
    }
  }
  persistable?(): object {
    return JSON.parse(JSON.stringify(this));
  }
}
