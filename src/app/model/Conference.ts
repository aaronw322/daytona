import ConferenceEntity from './entities/ConferenceEntity';

export default class Conference extends ConferenceEntity {
  constructor(entity: Conference) {
    super(entity);
  }
}
