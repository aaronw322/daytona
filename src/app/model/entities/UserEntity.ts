import Entity from '../interfaces/Entity';

export default class UserEntity extends Entity {
  firstName: string;
  lastName: string;
  email: string;
  constructor(entity: UserEntity) {
    super(entity);
    if (entity) {
      this.version = '1.0.0';
      this.firstName = entity.firstName;
      this.lastName = entity.lastName;
      this.email = entity.email;
    }
  }
  static collection(): string {
    return 'users';
  }
  static doc(id: string): string {
    return `${UserEntity.collection()}/${id}`;
  }
}
