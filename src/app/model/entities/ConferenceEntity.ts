import Entity from '../interfaces/Entity';

export default class ConferenceEntity extends Entity {
  name: string;
  status?: string;
  url?: string;
  links?: {
    participants: string,
    recordings: string
  };
  constructor(entity: ConferenceEntity) {
    super(entity);
    if (entity) {
      this.name = entity.name;
      this.status = entity.status;
      this.url = entity.url;
      this.links = entity.links;
    }
  }
  static collection(): string {
    return 'conferences';
  }
  static doc(id: string): string {
    return `${ConferenceEntity.collection()}/${id}`;
  }
}
