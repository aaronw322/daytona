export default class MutationEntity {
  by: string;
  timestamp: Date;
  constructor(entity: MutationEntity) {
    if (entity) {
      this.by = entity.by;
      this.timestamp = entity.timestamp;
    }
  }
}
