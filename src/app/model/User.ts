import UserEntity from './entities/UserEntity';

export default class User extends UserEntity {
  constructor(entity: UserEntity) {
    super(entity);
  }
}
