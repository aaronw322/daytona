import MutationEntity from './entities/MutationEntity';

export default class Mutation extends MutationEntity {
  constructor(entity: Mutation) {
    super(entity);
  }
}
