import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import Conference from '../model/Conference';
import {Observable} from 'rxjs';
import {AngularFireFunctions} from '@angular/fire/functions';
import { uniqueNamesGenerator, Config, starWars } from 'unique-names-generator';

@Injectable({
  providedIn: 'root'
})
export class ConferenceService {

  constructor(
    private afs: AngularFirestore,
    private fns: AngularFireFunctions
  ) { }

  /**
   * Conference creation is delegated to the cloud function `createConference`, which is defined in `serverless/functions/index.ts`
   * the only other useful thing this method does is generate a conference name if none was given.
   * @param name: string
   */
  async createConference(name: string): Promise<any> {
    // This is a favorite feature of mine. When firebase first came out, I had to cook up an HTTP request to call my cloud functions. Now
    // it's dead simple:
    const callable = this.fns.httpsCallable('createConference');
    let conferenceName;
    if (name) {
      console.log(`Name is [${name}]`);
      conferenceName = name;
    } else {
      const config: Config = {
        dictionaries: [starWars]
      };
      const characterName: string = uniqueNamesGenerator(config); // Han Solo
      console.log(`Character name: [${characterName}]`);
      conferenceName = `Conference with ${characterName}`;
    }
    await callable({ name: conferenceName }).toPromise();
  }

  /**
   * This is some sweet action.  It listens for any conference changes made to the database by way of an observable. One line of code
   * provides real time changes to all client sessions - no web socket implementation on the backend nor polling shenanigans on the front.
   */
  subscribeToConferences(): Observable<Conference[]> {
    return this.afs.collection<Conference>(Conference.collection()).valueChanges();
  }
}
