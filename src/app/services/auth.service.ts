import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import User from '../model/User';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import Mutation from '../model/Mutation';

/**
 * I've been accessing firebase directly in these services, but I have historically introduced another "DAO" service layer to create a seam
 * between firebase and the app so that it could be swapped out with any other restful implementation without much fuss (among other
 * benefits).
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<User>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    /**
     * Expose an authenticated user with an observable.
     */
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
    /**
     * As soon as an authenticated user is detected, route them to their conferences page. Ideally, it would also send the user to the
     * login page when it detected that the user is not authenticated, but I didn't get to that...
     */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        router.navigate([`/users/${user.uid}/conferences`]);
      }
    });
  }

  /**
   * Firebase manages all authentication concerns and provides an API to access the underlying JWT it maintains for advance use cases like
   * RBAC, etc.
   * @param email: string
   * @param password: string
   */
  async passwordSignIn(email: string, password: string): Promise<any> {
    // await this.afAuth.signInWithEmailAndPassword(email, password);
    return Promise.resolve('Login disabled');
  }

  /**
   * Firebase only maintains a handful of user fields, so anything else is persisted in our firestore (document-base/no-sql) database that
   * we design.
   * @param email: string
   * @param firstName: string
   * @param lastName: string
   * @param password: string
   */
  async createAccount({email, firstName, lastName, password}): Promise<any> {
    // Here we create a firebase managed user.
    const credential = await this.afAuth.createUserWithEmailAndPassword(email, password);
    // Here use use the primary key from the firebase user as the key for our firestore user that we manage.
    const userRef: AngularFirestoreDocument = await this.afs.doc(User.doc(credential.user.uid));
    const user = new User ({
      id: credential.user.uid,
      created: new Mutation({
        by: credential.user.uid,
        timestamp: new Date()
      }),
      email, firstName, lastName
    }).persistable();
    await userRef.set(user, {merge: true});
  }

  async signOut(): Promise<any> {
    await this.afAuth.signOut();
    return this.router.navigate(['/']);
  }
}
