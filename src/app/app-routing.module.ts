import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ConferenceComponent} from './conference/conference.component';
import {UserComponent} from './user/user.component';

const routes: Routes = [
  {path: 'users/:id/conferences', component: ConferenceComponent},
  {path: 'users/auth', component: UserComponent},
  {path: '**', redirectTo: 'users/auth', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
