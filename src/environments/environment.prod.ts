export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC_KIB5RWRWoLOqO-T4rj9stO2dZQPN0_s',
    authDomain: 'daytona-3t.firebaseapp.com',
    databaseURL: 'https://daytona-3t.firebaseio.com',
    projectId: 'daytona-3t',
    storageBucket: 'daytona-3t.appspot.com',
    messagingSenderId: '512237819854',
    appId: '1:512237819854:web:21db1c507e1ca2f3497d9f',
    measurementId: 'G-GV8G1NPKVE'
  }
};
