// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC_KIB5RWRWoLOqO-T4rj9stO2dZQPN0_s',
    authDomain: 'daytona-3t.firebaseapp.com',
    databaseURL: 'https://daytona-3t.firebaseio.com',
    projectId: 'daytona-3t',
    storageBucket: 'daytona-3t.appspot.com',
    messagingSenderId: '512237819854',
    appId: '1:512237819854:web:21db1c507e1ca2f3497d9f',
    measurementId: 'G-GV8G1NPKVE'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
