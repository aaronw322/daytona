import * as functions from 'firebase-functions';

const initialize = {
  firebase: false
};

/**
 * I should have broken out the implementation of each function into its own file, but I took a shortcut and just lumped it altogether
 * since it's not that much code.
 */
export const createConference = functions.https.onCall(async (data /*, context*/) => {
  // const Conference = await import('../../../src/app/model/Conference');
  type Conference = {
    name: string,
    id?: string
  }
  // We could import this globally, but since firebase instantiates each function separately, that might waste resources if another
  // function doesn't need the dependency.
  const admin = await import('firebase-admin');
  if (!initialize.firebase) {
    admin.initializeApp();
    initialize.firebase = true;
  }
  const persistedConference = await persistConference();
  await startTwilioConference(persistedConference);

  async function startTwilioConference(conference: Conference) {
    // These environment variables are managed by firebase, and I have an example of how they're configured in
    // `serverless/config/default.env.sh`.  You'll need to have the firebase cli installed to set them.
    const accountSid = functions.config().twilio.account.sid;
    const authToken = functions.config().twilio.auth.token;
    const client = require('twilio')(accountSid, authToken);
    await client.video.rooms
      .create({
        recordParticipantsOnConnect: true,
        // This callback is defined below as a webhook named `createConferenceWebhook`. The URL of the webhook is stored in firebase's
        // environment variables.
        statusCallback: `${functions.config().webhooks.conference.create}?cid=${conference.id}`,
        type: 'group',
        uniqueName: conference.name
      })
      .then(async (room: any) => {
        console.log(`Room SID: [${room.sid}]`);
        const conferenceDoc = admin.firestore().doc(`conferences/${conference.id}`);
        await conferenceDoc.update({
          status: room.status,
          url: room.url,
          links: room.links
        });
        await conferenceDoc.collection(`responses`).doc(`initial`).set(persistable(room));
      });
  }

  /**
   * This could have been done in the client and then handed off to the cloud function to handle twilio, but if there was some other
   * logic that we didn't want the user to see, it would make sense to do that here on the server.
   */
  async function persistConference() {
    const conf: Conference = {
      name: data.name
    };
    const conferenceDoc = await admin.firestore().collection(`conferences`).add(conf);
    await conferenceDoc.update({
      id: conferenceDoc.id
    });
    conf['id'] = conferenceDoc.id;
    return conf;
  }
});

/////////////////////////////////////////////
// Webhooks
/////////////////////////////////////////////

/**
 * All HTTP verbs are processed here. When any changes are made to Twilio's conference that we registered this webhook (callback) for,
 * they'll send the update to this endpoint. We could define an Express app with routes and parameters and the whole 9, but that's overkill
 * and a lot more work than what's needed for this.
 */
export const createConferenceWebhook = functions.https.onRequest(async (req, resp) => {
  const admin = await import('firebase-admin');
  if (!initialize.firebase) {
    admin.initializeApp();
    initialize.firebase = true;
  }
  console.log(`Body: [${req.body}].`);
  console.log(`Params: [${req.params}].`);
  if (req.params.cid) {
    const conferenceDoc = admin.firestore().doc(`conferences/${req.params.cid}`);
    if (req.body.status) {
      await conferenceDoc.update({
        status: req.body.status
      });
    }
    await conferenceDoc.collection(`responses`).add(persistable(req.body));
  } else {
    console.log(`Couldn't find CID query param.`);
  }
  resp.end();
});

/**
 * Firebase wants you to create converters for any class that you define to turn it into a plain json object to persist, but this hacky way
 * to accomplish that.
 * @param entity
 */
function persistable(entity: any): object {
  return JSON.parse(JSON.stringify(entity));
}
